---
title: Get Help
platform: acjs
product: acjs
category: help
subcategory: index
date: '2017-03-21'
---
# Get Help

* HipChat Room: **Atlassian Connect JS**
* JIRA: https://ecosystem.atlassian.net/projects/ACJS
* Confluence: https://extranet.atlassian.com/display/ECO/Atlassian+Connect+JS
