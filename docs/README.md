# Atlassian Connect JS developer documentation

The contents of this directory will be automatically published as an npm package on every build and show up on DAC Internal:

https://dac-internal.internal.domain.dev.atlassian.io/atlassian-connect-js

For more information about the documentation structure, please read the DAC Meta documentation:

https://devhub.atlassian.io/devhub/

## Running DAC locally

If you are interested in running a DAC server locally with the content of this
repository please follow instructions in the
[DAC README](https://bitbucket.org/atlassian/dac).

## Publishing

To publish this documentation, bump the version in the `package.json` and then run:

```bash
npm publish
```
