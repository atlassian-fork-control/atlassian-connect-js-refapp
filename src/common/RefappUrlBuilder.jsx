import ContextParametersDAO from '../product/contextparameters/ContextParametersDAO';

/**
 * Use this class as per the following example:
 * const myUrl = new RefappUrlBuilder().setBaseUrl('https://www.myapp.com').setQueryString('foo=bar').setModuleUrl('/my-web-panel');
 */
export default class RefappUrlBuilder {

  baseUrl = '';
  queryString = '';
  pathname = '';
  hash = '';
  parameterIdsToValues = {}

  /**
   * Set the base URL and return this builder. For an app, this is declared by baseUrl property of the app descriptor.
   * @param baseUrl example: https://www.somedomain.com
   * @returns {RefappUrlBuilder} this instance so other attributes can be set in a chained calling manner.
   */
  setBaseUrl = (baseUrl) => {
    this.baseUrl = baseUrl;
    return this;
  };

  /**
   * Set the query string and return this builder.
   * @param queryString standard query parameters excluding the leading question mark.
   * @returns {RefappUrlBuilder} this instance so other attributes can be set in a chained calling manner.
   */
  setQueryString = (queryString) => {
    if (queryString) {
      this.queryString = queryString;
    }
    return this;
  };

  /**
   * Set the pathname and return this builder.
   * @param pathname standard URL pathname.
   * @returns {RefappUrlBuilder} this instance so other attributes can be set in a chained calling manner.
   */
  setPathname = (pathname) => {
    if (pathname) {
      this.pathname = pathname;
    }
    return this;
  };

  /**
   * Set the hash and return this builder.
   * @param hash standard URL hash.
   * @returns {RefappUrlBuilder} this instance so other attributes can be set in a chained calling manner.
   */
  setHash = (hash) => {
    if (hash) {
      this.hash = hash;
    }
    return this;
  };

  /**
   * Appends the query string and return this builder.
   * @param queryString standard query parameters excluding the leading question mark.
   * @returns {RefappUrlBuilder} this instance so other attributes can be set in a chained calling manner.
   */
  appendQueryString = (queryString) => {
    if (queryString) {
      const separator = this.queryString ? '&' : '';
      this.queryString += separator + queryString;
    }
    return this;
  };

  /**
   * Set the module URL and return this builder.
   * @param moduleUrl the module URL which is declared as the 'url' field of the module within the app's descriptor.
   * @returns {RefappUrlBuilder} this instance so other attributes can be set in a chained calling manner.
   */
  setModuleUrl = (moduleUrl) => {
    const tempLocation = document.createElement("a");
    tempLocation.href = 'http://www.example.com' + moduleUrl;
    const modulePathname = tempLocation.pathname;
    const moduleHash = tempLocation.hash;
    const moduleQueryString = tempLocation.search ? tempLocation.search.substring(1) : '';
    this.setPathname(modulePathname);
    this.setHash(moduleHash);
    this.appendQueryString(moduleQueryString);
    this.addProductMockingParameters();
    return this;
  };

  addProductMockingParameters = () => {
    const items = ContextParametersDAO.getItems();
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      this.appendQueryString(item.key + '=' + item.value);
    }
  };

  setParameterIdsToValues = (parameterIdsToValues) => {
    if (parameterIdsToValues) {
      this.parameterIdsToValues = parameterIdsToValues;
    }
    return this;
  };

  substituteParameters = (text) => {
    for (let parameterId in this.parameterIdsToValues) {
      const value = this.parameterIdsToValues[parameterId];
      const pattern = '{' + parameterId + '}';
      text = text.replace(pattern, encodeURIComponent(value));
    }
    return text;
  };

  build = () => {
    let url = this.baseUrl;
    if (this.pathname) {
      url += this.substituteParameters(this.pathname);
    }
    if (this.queryString) {
      url += '?' + this.substituteParameters(this.queryString);
    }
    if (this.hash) {
      url += this.substituteParameters(this.hash);
    }
    return url;
  };

}
