import RefappUrlBuilder from './RefappUrlBuilder';

const extensionIdToKeys = (extensionId) => {
  const keys = extensionId.split('__');
  return {
    appKey: keys[0],
    key: keys[1]
  }
}

/**
 * Simulates the Confluence Content API for now.
 * Stores / retrieves nested iframe JSON and HTML
 */
export const storeNestedIframeJSON = connectIframeProps => {
};

export const getNestedIframeJSON = () => {
  const windowData = JSON.parse(window.name);
  const extensionKeys = extensionIdToKeys(windowData.extension_id);
  return {
    key: extensionKeys.key,
    addon_key: extensionKeys.appKey,
    origin: windowData.options.origin,
    hostOrigin: windowData.options.hostOrigin,
    url: new RefappUrlBuilder()
      .setBaseUrl(windowData.options.origin)
      .setQueryString(window.location.search)
      .setModuleUrl('/#/app/refapp-app-home/')
      .build(),
    options: Object.assign({}, windowData.options, {
      targets: {
        env: {
          resize: 'both'
        }
      },
      hostFrameOffset: windowData.options.hostFrameOffset ? windowData.options.hostFrameOffset + 1 : 2
    })
  }
};
