export default class PerformanceappUtil {
  static currentTime(){
    return window.performance && window.performance.now ? window.performance.now() : new Date().getTime();
  }
}
