import * as React from 'react';
import { getNestedIframeJSON } from '../ProductAPIMock';
import { appMinWidth, appMinHeight } from '../CommonConstants';

/* global AP */

/**
 * This simulates the iframe-insertion.js web resource for now
 * see: ACJS-649
 */
class IframeInsertion extends React.PureComponent {
  componentWillMount() {
    this.iframe = AP.subCreate(getNestedIframeJSON());
  }

  render() {
    return (
      <iframe
        frameBorder={'0'}
        id={this.iframe.id}
        src={this.iframe.src}
        name={this.iframe.name}
        title={this.iframe.title}
        width={appMinWidth}
        height={appMinHeight}
      />
    );
  }
}

export default IframeInsertion;