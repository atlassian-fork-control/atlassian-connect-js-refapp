import React, { PureComponent } from 'react';

import AppDAO from "./AppDAO";
import FieldTextArea from '@atlaskit/field-text-area';

export default class AppDescriptorEditor extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      app: props.app,
      edited: false,
      valid: true
    };
  }

  handleDescriptorEdit = (event) => {
    event.preventDefault();
    try {
      const originalAppKey = this.state.app.key;
      const text = event.target.value;
      const updatedApp = JSON.parse(text);
      if (this.state.app.key !== updatedApp.key) {
        AppDAO.ensureKeyIsUnique(updatedApp);
      }
      AppDAO.updateApp(updatedApp, originalAppKey);
      this.setState({
        edited: true,
        valid: true
      });
      if (this.props.onAppSettingsChange) {
        // Note that it is necessary to pass the event through to keep redux happy.
        this.props.onAppSettingsChange(event);
      }
    } catch (exception) {
      this.setState({
        edited: true,
        valid: false
      });
    }
  }

  render() {
    var descriptorJson = JSON.stringify(this.state.app, null, 2);
    return (
      <div
        style={{
          width: '100%',
        }}
      >
        <FieldTextArea
          value={descriptorJson}
          autoFocus label={this.state.app.name + ' app descriptor'}
          minimumRows={20}
          enableResize={false}
          shouldFitContainer={true}
          isSpellCheckEnabled={false}
          isInvalid={!this.state.valid}
          onChange={(event) => this.handleDescriptorEdit(event)}
        />
      </div>
    );
  }
}
