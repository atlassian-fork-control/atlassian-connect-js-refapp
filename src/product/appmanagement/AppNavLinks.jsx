import React, { PureComponent } from 'react';

import AddonIcon from '@atlaskit/icon/glyph/addon';
import AppDAO from "./AppDAO";
import { Link } from 'react-router-dom';
import InlineDialogs from '../modules/InlineDialogs';
import { AkNavigationItem } from '@atlaskit/navigation';

export class AppLinkUtil {

  static pathPrefix = '/generic-app-host-page-';
  static appModuleSeparator = '__';

  static buildAppLink(app, module) {
    return AppLinkUtil.pathPrefix + app.key + AppLinkUtil.appModuleSeparator + module.key;
  }

  static parseAppLinkFromHash(hash) {
    const pathWithoutHash = hash.substring(1);
    const strippedPath = pathWithoutHash.replace(AppLinkUtil.pathPrefix, '');
    const parts = strippedPath.split(AppLinkUtil.appModuleSeparator);
    const appKey = parts[0];
    const moduleKey = parts[1];
    return {appKey: appKey, key: moduleKey};
  }

}

export default class AppNavLinks extends PureComponent {

  render() {
    const router = this.props.router;
    const showKitchenSinkInlineDialogs = this.props.showKitchenSinkInlineDialogs && AppDAO.isRefappEnabled();
    return (
      <div>
        {
          AppDAO.iterateModules(AppDAO.enabledAppsFilter, (app, moduleType, module) => {
            if (moduleType === this.props.moduleType) {
              const appPageUrl = AppLinkUtil.buildAppLink(app, module);
              return (
                <Link key={app.key + module.key} to={appPageUrl}>
                  <AkNavigationItem
                    icon={<AddonIcon label='Add-on' />}
                    text={module.name.value}
                    isSelected={router.route.location.pathname === appPageUrl}
                  />
                </Link>
              );
            } else {
              return null;
            }
          })
        }
        {
          showKitchenSinkInlineDialogs ?
            <InlineDialogs
              settings={this.props.settings}
              actions={this.props.actions}
            /> : null
        }
      </div>
    );
  }

}
