import * as React from 'react';
import {mount} from 'enzyme';
import { AtlasKitFlagView, AtlasKitFlagProvider } from '../AtlasKitFlagProvider';
import { FlagProviderValidator } from '@atlassian/connect-module-core';

/**
 * This test validates our implementation of the FlagProvider. The validation logic is owned
 * by connect-module-core.
 */
describe('FlagProviderValidationTesting', () => {

  it('Should implement the FlagProvider API properly.', () => {
    const flagProvider = new AtlasKitFlagProvider();
    mount(<AtlasKitFlagView flagProvider={flagProvider} />);
    FlagProviderValidator.validate(flagProvider);
  });

});