import * as React from 'react';
import {mount} from 'enzyme';
import { AtlasKitDialogView, AtlasKitDialogProvider } from '../AtlasKitDialogProvider';
import { DialogProviderValidator, MockConnectHost } from '@atlassian/connect-module-core';

/**
 * This test validates our implementation of the DialogProvider. The validation logic is owned
 * by connect-module-core.
 */
describe('DialogProviderValidationTesting', () => {

  it('Should implement the DialogProvider API properly.', () => {
    const dialogProvider = new AtlasKitDialogProvider();
    const settings = {
      timeoutAppIframes: false
    };
    const connectHost = new MockConnectHost();
    mount(<AtlasKitDialogView
      dialogProvider={dialogProvider}
      settings={settings}
      connectHost={connectHost} />);
    DialogProviderValidator.validate(dialogProvider);
  });

});