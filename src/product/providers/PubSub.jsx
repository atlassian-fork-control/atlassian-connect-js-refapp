export default class PubSub {

  static publishEvent(payload) {
    var event = new CustomEvent('webItemClick', {'detail': payload});
    var pubSub = document.getElementById('app-root');
    pubSub.dispatchEvent(event);
  }

  static subscribe(listener) {
    var pubSub = document.getElementById('app-root');
    pubSub.addEventListener('webItemClick', listener, false);
  }

}