import { DropdownProvider } from '@atlassian/connect-module-core';
import React, { PureComponent } from 'react';
import DropdownView from '../components/connect/DropdownView';
import ProductDropdownProxy from "./proxies/ProductDropdownProxy";

export class AtlasKitDropdownProvider implements DropdownProvider {

  dropdownView = null;

  constructor(props) {
    this.state = {
      dropdownIdsToProxies: {},
      activeDropdown: false,
      itemNotificationCallback: false,
    };
  }

  create(dropdownProviderCreationOptions, callbackContext) {
    return this.dropdownView.createDropdown(dropdownProviderCreationOptions, callbackContext);
  }

  showAt(dropdownProviderCreationOptions, iframeOptions) {
    this.dropdownView.showAt(dropdownProviderCreationOptions, iframeOptions);
  }

  hide(dropdownId) {
    this.dropdownView.hide(dropdownId);
  }

  itemEnable(dropdownId, itemId) {
    this.dropdownView.itemEnable(dropdownId, itemId, true);
  }

  itemDisable(dropdownId, itemId) {
    this.dropdownView.itemEnable(dropdownId, itemId, false);
  }

  setView = (dropdownView) => {
    this.dropdownView = dropdownView;
  }

  destroyByExtension(extension_id) {
    let dropdownIds = Object.getOwnPropertyNames(this.dropdownView.state.dropdownIdsToProxies);

    dropdownIds.forEach((dropdownId) => {
      let extensionCallback = this.dropdownView.state.dropdownIdsToProxies[dropdownId].state.dropdownProvider;
      if(extensionCallback.extension_id === extension_id) {
        this.dropdownView.destroyDropdown(dropdownId);
      }
    });
  }

}

export class AtlasKitDropdownView extends PureComponent {

  state = {
    counter: 1,
    dropdownIdsToProxies: {},
    activeDropdown: null,
    allDropdownCreationOptions: [], // using an array keeps the allDropdownCreationOptions ordered
    itemNotificationCallback: null
  };

  componentDidMount() {
    this.props.dropdownProvider.setView(this);
  }

  componentDidUpdate() {
    this.props.dropdownProvider.setView(this);
  }

  itemEnable(dropdownId, itemId, isEnabled) {
    if (dropdownId) {
      const dropdownProxy = this.state.dropdownIdsToProxies[dropdownId];
      if (dropdownProxy) {
        dropdownProxy.itemEnabled(itemId, isEnabled);
        if (this.state.activeDropdown && this.state.activeDropdown.state.dropdownId === dropdownId) {
          this.setState({
            activeDropdown: dropdownProxy
          });
        }
      }
    }
  }

  /**
   * @param dropdownProviderCreationOptions - for type info, see @atlassian/connect-module-core/lib/modules/dropdown/DropdownCreationOptions.
   */
  createDropdown = (dropdownProviderCreationOptions, callbackContext) => {
    const dropdownProxy = new ProductDropdownProxy(dropdownProviderCreationOptions, callbackContext, this);
    const thisDropdownCreationOptions = {
      'dropdownId': dropdownProviderCreationOptions.dropdownId,
      'dropdownGroups': dropdownProviderCreationOptions.dropdownGroups,
      'itemNotificationCallback': dropdownProviderCreationOptions.itemNotificationCallback
    };
    const dropdownIdsToProxies = Object.assign({}, this.state.dropdownIdsToProxies);
    dropdownIdsToProxies[dropdownProviderCreationOptions.dropdownId] = dropdownProxy;
    this.setState({
      dropdownIdsToProxies: dropdownIdsToProxies,
      allDropdownCreationOptions: this.state.allDropdownCreationOptions.concat([thisDropdownCreationOptions])
    });
    const dropdownHandle = {
      id: thisDropdownCreationOptions.dropdownId,
      close: () => {
        this.closeDropdown(thisDropdownCreationOptions.dropdownId)
      }
    };
    var that = this;
    window.addEventListener('popstate', function(){
      that.destroyDropdown(thisDropdownCreationOptions.dropdownId);
      window.removeEventListener('popstate', this);
    });

    return dropdownHandle;
  };

  showAt = (dropdownProviderCreationOptions, iframeOptions) => {
    if (dropdownProviderCreationOptions.dropdownId) {
      const x = dropdownProviderCreationOptions.x + iframeOptions.iframeDimensions.left;
      const y = dropdownProviderCreationOptions.y + iframeOptions.iframeDimensions.top;
      const dropdownProxy = this.state.dropdownIdsToProxies[dropdownProviderCreationOptions.dropdownId];
      if (dropdownProxy) {
        dropdownProxy.showAt(x, y, dropdownProviderCreationOptions.width);
        console.log('Showing dropdown at ', x, y, '...');
        this.setState({
          counter: this.state.counter + 1,
          activeDropdown: dropdownProxy
        });
      } else {
        console.error('dropdown not found with the following id: ', dropdownProviderCreationOptions.dropdownId);
      }
    }
  }

  hide = (dropdownId) => {
    if (dropdownId) {
      const dropdownProxy = this.state.dropdownIdsToProxies[dropdownId];
      if (dropdownProxy) {
        dropdownProxy.hide();
        if (this.state.activeDropdown && this.state.activeDropdown.state.dropdownId === dropdownId) {
          this.setState({
            counter: this.state.counter + 1,
            activeDropdown: null
          });
        }
      }
    }
  }

  closeDropdown = (dropdownId) => {
    this.state.allDropdownCreationOptions.forEach(function(dropdownCreationOptions) {
      if (dropdownCreationOptions.dropdownId === dropdownId && dropdownCreationOptions.onClose) {
        dropdownCreationOptions.onClose(dropdownId);
      }
    });

    var newDropdowns = this.state.allDropdownCreationOptions.filter(function(dropdownCreationOptions) {
      return dropdownCreationOptions.dropdownId !== dropdownId;
    });
    this.setState({
      allDropdownCreationOptions: newDropdowns
    });
  };

  // clean up references to a dropdown menu when an extension is destroyed
  destroyDropdown = (dropdownId) => {
    //close and remove from state.allDropdownCreationOptions and DropdownView
    this.closeDropdown(dropdownId);
    // remove from dropdownIdsToProxies
    let dropdownIdsToProxies = Object.assign({}, this.state.dropdownIdsToProxies);
    delete dropdownIdsToProxies[dropdownId];

    // remove from activeDropdown if appropriate
    let activeDropdown = this.state.activeDropdown;
    if(this.state.activeDropdown.state.dropdownId === dropdownId) {
      activeDropdown = null;
    }
    this.setState({
      dropdownIdsToProxies: dropdownIdsToProxies,
      activeDropdown: activeDropdown
    });
  }

  render() {
    return (
      <DropdownView
        counter={this.state.counter}
        activeDropdown={this.state.activeDropdown}
        allDropdownCreationOptions={this.state.allDropdownCreationOptions}
        actions={{
          dismissDropdown: this.closeDropdown
        }}
        onDismissed={this.closeDropdown}
      />
    );
  }
}
