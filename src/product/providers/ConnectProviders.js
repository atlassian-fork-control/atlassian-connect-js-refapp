import { DefaultInlineDialogProvider } from '@atlassian/connect-module-core';

import connectHost from 'atlassian-connect-js';
import { logger } from '@atlassian/connect-module-core';
import { analytics } from '@atlassian/connect-module-core';

import AppProvider from './AppProvider';
import { AtlasKitDialogProvider } from './AtlasKitDialogProvider';
import { AtlasKitDropdownProvider } from './AtlasKitDropdownProvider';
import { AtlasKitFlagProvider } from './AtlasKitFlagProvider';
import { AtlasKitMessageProvider } from './AtlasKitMessageProvider';
import ProductLoggerImpl from '../components/ProductLoggerImpl';
import ProductAnalyticsImpl from '../components/ProductAnalyticsImpl';

class ConnectProviders {

  constructor() {
    // There could conceivably be logic here to choose the right framework adaptor to use, however,
    // at this stage we only have one plausable implementation (ACJS) which is provided by default
    // by the connect host.
    this.adaptor = connectHost.getFrameworkAdaptor();
    this.dialogProvider = new AtlasKitDialogProvider();
    this.dropdownProvider = new AtlasKitDropdownProvider();
    this.flagProvider = new AtlasKitFlagProvider();
    this.inlineDialogProvider = new DefaultInlineDialogProvider();
    this.messageProvider = new AtlasKitMessageProvider();
    this.appProvider = new DefaultInlineDialogProvider();
    connectHost.registerProvider('addon', AppProvider);
    logger.registerLoggerAPI(new ProductLoggerImpl());
    analytics.registerAnalyticsAPI(new ProductAnalyticsImpl());
  };

}

export default new ConnectProviders();
