import React, { PureComponent } from 'react';
import RefappUtil from '../../common/RefappUtil';
import DialogView from '../components/connect/DialogView';
import DefaultConnectIframeProvider from './../modules/DefaultConnectIframeProvider';

export class AtlasKitDialogProvider implements DialogProvider {

  dialogView = null;

  /**
   * @param dialogCreationOptions - for type info, see @atlassian/connect-module-core/lib/modules/dialog/DialogCreationOptions.
   */
  create = (dialogCreationOptions, dialogExtension) => {
    console.log('AtlasKitDialogProvider:create ', dialogCreationOptions);
    console.log('AtlasKitDialogProvider:create ', JSON.stringify(dialogExtension));
    return this.dialogView.createDialog(dialogCreationOptions, dialogExtension);
  };

  createButton = action => {
    console.log('AtlasKitDialogProvider:createButton ', action);
    this.dialogView.createButton(action);
  };

  close = () => {
    console.log('AtlasKitDialogProvider:close');
    this.dialogView.onClose();
  };

  setButtonDisabled = (identifier, disabled) => {
    console.log(`AtlasKitDialogProvider:setButtonDisabled: (${identifier}, ${disabled})`);
    this.dialogView.setButtonDisabled(identifier, disabled);
  };

  setButtonHidden = (identifier, hidden) => {
    console.log(`AtlasKitDialogProvider:setButtonHidden: (${identifier}, ${hidden})`);
    this.dialogView.setButtonHidden(identifier, hidden);
  };

  isButtonDisabled = identifier => {
    return this.dialogView.isButtonDisabled(identifier) ;
  };

  isButtonHidden = identifier => {
    return this.dialogView.isButtonHidden(identifier) ;
  };

  toggleButton = identifier => {
    console.log('AtlasKitDialogProvider:toggleButton: ', identifier);
    this.dialogView.toggleButton(identifier) ;
  };

  isActiveDialog = addon_key => {
    return this.dialogView.isActiveDialog(addon_key);
  };

  //private
  setView = dialogView => {
    this.dialogView = dialogView;
  };

}

export class AtlasKitDialogView extends PureComponent {

  state = {
    dialogs: [],
    connectIframeProvider: new DefaultConnectIframeProvider()
  };

  assertAnyActiveDialogOrThrow = () => {
    if (this.state.dialogs.length === 0) {
      throw new Error('Failed to find any active dialog');
    }
  }

  onClose = () => {
    this.assertAnyActiveDialogOrThrow();
    this.setState({
      dialogs: this.state.dialogs.slice(0, this.state.dialogs.length - 1)
    });
  };

  onDialogDismissed = e => {
    this.assertAnyActiveDialogOrThrow();
    const escapeKeyPressed = typeof e.key !== undefined && e.key === 'Escape';
    const keepOpen =
      !escapeKeyPressed ||
      (
        escapeKeyPressed &&
        (typeof this.state.dialogs[this.state.dialogs.length-1].closeOnEscape !== undefined &&
          this.state.dialogs[this.state.dialogs.length-1].closeOnEscape === false)
      );
    if (keepOpen) {
      return;
    } else {
      this.onClose();
    }
  };

  compareButtons = (a,b) => {
    if (a.identifier === 'cancel') {
      return 1;
    } else if (a.identifier === 'submit') {
      if (b.identifier === 'cancel') {
        return -1;
      } else {
        return 1;
      }
    }
    return 0;
  };

  compareDialog = extension => {
    return dialog => {
      return ((dialog.extension.addon_key === extension.addon_key) && (dialog.extension.key === extension.key));
    };
  };

  convertFromJSAPI = options => {
    options.isOpen = true;
    options.actions.sort(this.compareButtons);
    options.actions.map(action => action.hidden = action.hidden || false);
    options.actions.map(action => action.disabled = action.disabled || false);
    options.actions.map(action => action.key = action.identifier);
    options.width = options.width || options.size;
    options.height = options.height || options.size;
    return options;
  };

  /**
   * @param dialogCreationOptions - for type info, see @atlassian/connect-module-core/lib/modules/flag/DialogCreationOptions.
   */
  createDialog = (options, extension) => {
    if (this.state.dialogs.filter(this.compareDialog(extension)).length > 0) {
      console.warn('Dialog with addon_key: ' + extension.addon_key + ' ,key: ' + extension.key + ' already exists');
    }

    RefappUtil.getContentResolver(this.props.settings)(extension)
    .then(iframeContext => {
      this.setState({
        dialogs: [...this.state.dialogs, Object.assign({},{
          options: this.convertFromJSAPI(options),
          extension: Object.assign(extension, {url: iframeContext.url})
        })]
      });
    });
  };

  createButton = action => {
    this.assertAnyActiveDialogOrThrow();
    this.setState({
      dialogs: this.state.dialogs.map((dialog, index) => {
        if (index === (this.state.dialogs.length - 1)) {
          return Object.assign({}, dialog, {
            options: Object.assign({}, dialog.options, {
              actions: [
                Object.assign({}, action, {
                  key: action.identifier,
                  hidden: false,
                  disabled: false
                }),
                ...dialog.options.actions
              ]
            })
          });
        }
        return dialog;
      })
    });
  };

  setButtonDisabled = (identifier, disabled) => {
    this.assertAnyActiveDialogOrThrow();
    this.setState({
      dialogs: this.state.dialogs.map((dialog, index) => {
        if (index === (this.state.dialogs.length - 1)) {
          return Object.assign({}, dialog, {
            options: Object.assign({}, dialog.options, {
              actions: dialog.options.actions.map(action => identifier === action.identifier ? Object.assign({}, action, {disabled}) : action)
            })
          });
        }
        return dialog;
      })
    });
  };

  setButtonHidden = (identifier, hidden) => {
    this.assertAnyActiveDialogOrThrow();
    this.setState({
      dialogs: this.state.dialogs.map((dialog, index) => {
        if (index === (this.state.dialogs.length - 1)) {
          return Object.assign({}, dialog, {
            options: Object.assign({}, dialog.options, {
              actions: dialog.options.actions.map(action => identifier === action.identifier ? Object.assign({}, action, {hidden}) : action)
            })
          });
        }
        return dialog;
      })
    });
  };

  isButtonDisabled = identifier => {
    this.assertAnyActiveDialogOrThrow();
    let action =  this.state.dialogs.slice(-1)[0].options.actions.filter(action => action.identifier === identifier)[0];
    if (action) {
      return action.disabled;
    }
    return false;
  };

  isButtonHidden = identifier => {
    this.assertAnyActiveDialogOrThrow();
    let action = this.state.dialogs.slice(-1)[0].options.actions.filter(action => action.identifier === identifier)[0];
    if (action) {
      return action.hidden;
    }
    return false;
  };

  toggleButton = identifier => {
    this.assertAnyActiveDialogOrThrow();
    this.setState({
      dialogs: this.state.dialogs.map((dialog, index) => {
        if (index === (this.state.dialogs.length - 1)) {
          return Object.assign({}, dialog, {
            options: Object.assign({}, dialog.options, {
              actions: dialog.options.actions.map(action => identifier === action.identifier ? Object.assign({}, action, {
                disabled: !action.disabled
              }) : action)
            })
          });
        }
        return dialog;
      })
    });
  };

  isActiveDialog = addon_key => {
    this.assertAnyActiveDialogOrThrow();
    return this.state.dialogs.slice(-1)[0].extension.addon_key === addon_key;
  };

  componentWillMount() {
    this.props.dialogProvider.setView(this);
  };

  render() {
    return (
      <DialogView
        dialogs={this.state.dialogs}
        settings={this.props.settings}
        connectHost={this.props.connectHost}
        onDialogDismissed={this.props.onDialogDismissed}
        connectIframeProvider={this.state.connectIframeProvider}
      />
    );
  };
}
