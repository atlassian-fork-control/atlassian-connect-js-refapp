import React, { PureComponent } from 'react';

import ContextParametersDAO from "./ContextParametersDAO";
import FieldTextArea from '@atlaskit/field-text-area';

export default class ContextParameterEditor extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      itemValue: props.item.value,
      edited: false,
      valid: true
    };
  }

  handleItemEdit = (event) => {
    event.preventDefault();
    try {
      const newValue = event.target.value;
      this.props.item.value = newValue;
      ContextParametersDAO.updateItem(this.props.item.key);
      this.setState({
        itemValue: newValue,
        edited: true,
        valid: true
      });
      if (this.props.onItemChange) {
        // Note that it is necessary to pass the event through to keep redux happy.
        this.props.onItemChange(event);
      }
    } catch (exception) {
      this.setState({
        edited: true,
        valid: false
      });
    }
  }

  render() {
    return (
      <div
        style={{
          width: '100%',
        }}
      >
        <FieldTextArea
          value={this.state.itemValue}
          autoFocus label={this.props.item.key}
          minimumRows={5}
          enableResize={false}
          shouldFitContainer={true}
          isSpellCheckEnabled={false}
          isInvalid={!this.state.valid}
          onChange={(event) => this.handleItemEdit(event)}
        />
      </div>
    );
  }
}
