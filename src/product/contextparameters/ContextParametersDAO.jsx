import ContextParameter from './ContextParameter';

export default class ContextParametersDAO {

  static itemsKey = "refapp-product-contextparameters";

  static getItems() {
    let items = null;
    let json = localStorage.getItem(ContextParametersDAO.itemsKey);
    if (json) {
      items = JSON.parse(json);
    } else {
      items = ContextParametersDAO.getDefaultItems();
    }
    return items;
  }

  static setItems(items) {
    const json = JSON.stringify(items);
    localStorage.setItem(ContextParametersDAO.itemsKey, json);
  }

  static updateItem(itemToUpdate) {
    const items = ContextParametersDAO.getItems();
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      if (item.key === itemToUpdate.key) {
        item.value = itemToUpdate.value;
      }
    }
    ContextParametersDAO.setItems(items);
  }

  static resetToDefault() {
    const items = ContextParametersDAO.getDefaultItems();
    ContextParametersDAO.setItems(items);
    return items;
  }

  static getDefaultItems() {
    let items = [];
    this.addDefaultItemsContributedByConnectPlugin(items);
    return items;
  }

  static addDefaultItemsContributedByConnectPlugin(items) {
    items.push(new ContextParameter('connect', 'true'));
    items.push(new ContextParameter('tz', 'Etc/GMT'));
    items.push(new ContextParameter('loc', 'en-GB'));
    items.push(new ContextParameter('xdm_e', 'https://some-tenant.atlassian.net'));
    items.push(new ContextParameter('xdm_c', 'channel-[namespace]'));
    items.push(new ContextParameter('cp', '/wiki'));
    items.push(new ContextParameter('user_key', '[some-user-key]'));
    items.push(new ContextParameter('user_id', 'admin'));
  }

}
