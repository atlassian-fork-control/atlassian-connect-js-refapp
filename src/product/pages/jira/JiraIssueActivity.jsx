import React, { PureComponent } from 'react';
import Avatar from "@atlaskit/avatar";
import Comment, {
  CommentAuthor,
  CommentTime,
  CommentAction,
  CommentEdited,
} from "@atlaskit/comment";

export default class JiraIssueActivity extends PureComponent {

  state = {
  };

  handleCommentClick = (event) => {
    console.log(`${event.target.textContent} was clicked.`);
  };

  render() {
    return (
      <div>
        <h5>Activity:</h5>
        {
          this.props.issue.comments.map(comment => (
            <Comment
              key={comment.key}
              avatar={<Avatar src={comment.avatarImg} label="Issue avatar" size="medium" />}
              author={<CommentAuthor href="/author">{comment.authorName}</CommentAuthor>}
              type={comment.type}
              edited={<CommentEdited>Edited</CommentEdited>}
              restrictedTo={comment.restrictions}
              time={<CommentTime>{comment.time}</CommentTime>}
              content={
                <p>
                  {comment.text}
                </p>
              }
              actions={[
                <CommentAction onClick={this.handleCommentClick}>Reply</CommentAction>,
                <CommentAction onClick={this.handleCommentClick}>Edit</CommentAction>,
                <CommentAction onClick={this.handleCommentClick}>Like</CommentAction>,
              ]}
            />
          ))
        }
      </div>
    );
  }
}
