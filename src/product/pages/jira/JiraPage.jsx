import React, { PureComponent } from 'react';
import ContentWrapper from "../../components/ContentWrapper";
import JiraAdmin from "./JiraAdmin";
import JiraIssueDialog from "./JiraIssueDialog";
import AppDAO from "../../appmanagement/AppDAO";
import MockJiraIssueData from "./MockJiraIssueData";
import PageTitle from "../../components/PageTitle";
import styled from "styled-components";
import Tabs from "@atlaskit/tabs";

export default class JiraPage extends PureComponent {

  state = {
    selectedIndex: 0
  };

  buildTabDefinitions = () => {
    this.tabs = [
      {label: 'Admin', renderer: this.renderAdminContent()},
      {label: 'Issue Dialog', renderer: this.renderIssueDialogContent(), defaultSelected: true}
    ];
    const tabDefinitions = this.tabs.map((tabMetaData) => {
      return {
        label: tabMetaData.label,
        content: tabMetaData.renderer,
        defaultSelected: tabMetaData.defaultSelected
      };
    });
    return tabDefinitions;
  };

  buildTabs = () => this.buildTabDefinitions().map((tab, index) => ({
    ...tab,
    isSelected: index === this.state.selectedIndex
  }));

  handleTabSelection = (selectedTabIndex) => {
    this.setState({ selectedIndex: selectedTabIndex });
    console.log('Selected tab', selectedTabIndex + 1);
  };

  renderAdminContent = () => {
    return this.renderTabContent(<JiraAdmin />);
  };

  renderIssueDialogContent = () => {
    const webPanelModules = [];
    AppDAO.iterateModules(AppDAO.enabledAppsFilter, (app, moduleType, module) => {
      if (moduleType === 'webPanels') {
        webPanelModules.push(module);
      }
    });
    const issue = MockJiraIssueData.getMockJiraIssue();
    return this.renderTabContent(
      <JiraIssueDialog
        webPanelModules={webPanelModules}
        issue={issue}
      />
    );
  };

  renderTabContent = (content) => {
    const ContentContainer = styled.div`
      margin-top: 20px;
      padding: 0px;
    `;
    return (
      <ContentContainer>
        {content}
      </ContentContainer>
    );
  };

  render() {
    return (
      <ContentWrapper>
        <PageTitle>Jira</PageTitle>
        <Tabs
          tabs={this.buildTabs()}
          onSelect={this.handleTabSelection}
        />
      </ContentWrapper>
    );
  }
}
