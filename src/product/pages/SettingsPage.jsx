import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AppManagementSettings from '../appmanagement/AppManagementSettings.jsx';
import Toggle from '@atlaskit/toggle';
import { Label } from '@atlaskit/field-base'; //TODO: AK-1621
import InfoIcon from '@atlaskit/icon/glyph/info';
import Button from '@atlaskit/button';
import ContentWrapper from '../components/ContentWrapper';
import PageTitle from '../components/PageTitle';
import * as settingsActions from '../actions/settings'
import { appOptionsURL } from '../constants';
import Select from '@atlaskit/single-select';
import MultiSelect from '@atlaskit/multi-select';
import ContextParameterSettings from '../contextparameters/ContextParameterSettings';

const themes = [
  {
    items: [
      { content: 'Light', value: 'light' },
      { content: 'Dark', value: 'dark' },
    ],
  },
];

const atlaskit_modules = [
  {
    items: [
      { content: 'flag', value: 'flag'},
      { content: 'dialog', value: 'dialog'},
      { content: 'dropdown', value: 'dropdown'},
      { content: 'inlineDialog', value: 'inlineDialog'},
      { content: 'messages', value: 'messages'},
    ]
  }
]

class SettingsPage extends PureComponent {

  static propTypes = {
    settings: PropTypes.shape({
      updateAppSettings: PropTypes.bool,
      autoresize: PropTypes.bool,
      widthinpx: PropTypes.bool,
      isFullPage: PropTypes.bool,
      resize: PropTypes.bool,
      sizeToParent: PropTypes.bool,
      margin: PropTypes.bool,
      base: PropTypes.bool,
      atlaskit: PropTypes.array,
      timeoutAppIframes: PropTypes.bool,
      productTheme: PropTypes.string,
      appTheme: PropTypes.string
    }),
    actions: PropTypes.shape({
      updateAppSettings: PropTypes.func,
      updateAutoResize: PropTypes.func,
      updateWidthInPx: PropTypes.func,
      updateIsFullPage: PropTypes.func,
      updateResize: PropTypes.func,
      updateSizeToParent: PropTypes.func,
      updateMargin: PropTypes.func,
      updateBase: PropTypes.func,
      updateAtlaskit: PropTypes.func,
      updateTimeoutAppIframes: PropTypes.func,
      updateProductTheme: PropTypes.func,
      updateAppTheme: PropTypes.func
    })
  };

  render() {
    return (
      <ContentWrapper>
        <PageTitle>Settings</PageTitle>
        <h2>App Settings</h2>
        <AppManagementSettings
          onAppSettingsChange={this.props.actions.updateAppSettings}
        />

        <h2>Context Parameters</h2>
        <ContextParameterSettings
          onAppSettingsChange={this.props.actions.updateAppSettings}
        />

        <h2>Product Options</h2>
        <div>
          <Label label='autoresize' />
          <Toggle
            label='autoresize'
            isChecked={this.props.settings.autoresize}
            isDefaultChecked={this.props.settings.autoresize}
            onChange={this.props.actions.updateAutoResize}
          />
        </div>
        <div>
          <Label label='widthinpx' />
          <Toggle
            label='widthinpx'
            isChecked={this.props.settings.widthinpx}
            isDefaultChecked={this.props.settings.widthinpx}
            onChange={this.props.actions.updateWidthInPx}
          />
        </div>
        <div>
          <Label label='isFullPage' />
          <Toggle
            label='isFullPage'
            isChecked={this.props.settings.isFullPage}
            isDefaultChecked={this.props.settings.isFullPage}
            onChange={this.props.actions.updateIsFullPage}
          />
        </div>
        <div>
          <Label label='Enable Atlaskit' />
          <MultiSelect
            defaultSelected={this.props.settings.atlaskit}
            items={atlaskit_modules}
            onSelectedChange={this.props.actions.updateAtlaskit}
          />
        </div>
        <div>
          <Label label='Timeout App iframes' />
          <Toggle
            label='Timeout App iframes'
            isChecked={this.props.settings.atlaskit}
            isDefaultChecked={this.props.settings.timeoutAppIframes}
            onChange={this.props.actions.updateTimeoutAppIframes}
          />
        </div>
        <div>
          <Label label='Use expired JWT' />
          <Toggle
            label='Use expired JWT'
            isChecked={this.props.settings.expiredJWT}
            isDefaultChecked={this.props.settings.expiredJWT}
            onChange={this.props.actions.updateExpiredJWT}
          />
        </div>
        <div>
          <Select
            items={themes}
            label="Product Theme"
            defaultSelected={
              themes[0].items.find(item => item.value === this.props.settings.productTheme)
              || themes[0].items[0]}
            onSelected={(item) => {
              this.props.actions.updateProductTheme(item.item.value);
              // Workaround for AK-4198
              window.location.reload();
            }}
          />
        </div>
        <h2>App Options</h2>
        <br />
        <div>
          <Button
            appearance='subtle-link'
            iconBefore={<InfoIcon label='' />}
            target='_blank'
            href={appOptionsURL}
            >Documentation
          </Button>
        </div>
        <div>
          <Label label='resize' />
          <Toggle
            label='resize'
            isChecked={this.props.settings.resize}
            isDefaultChecked={this.props.settings.resize}
            onChange={this.props.actions.updateResize}
          />
        </div>
        <div>
          <Label label='sizeToParent' />
          <Toggle
            label='sizeToParent'
            isChecked={this.props.settings.sizeToParent}
            isDefaultChecked={this.props.settings.sizeToParent}
            onChange={this.props.actions.updateSizeToParent}
          />
        </div>
        <div>
          <Label label='margin' />
          <Toggle
            label='margin'
            isChecked={this.props.settings.margin}
            isDefaultChecked={this.props.settings.margin}
            onChange={this.props.actions.updateMargin}
          />
        </div>
        <div>
          <Label label='base' />
          <Toggle
            label='base'
            isChecked={this.props.settings.base}
            isDefaultChecked={this.props.settings.base}
            onChange={this.props.actions.updateBase}
          />
        </div>
        <div>
          <Select
            items={themes}
            label="App Theme"
            defaultSelected={
              themes[0].items.find(item => item.value === this.props.settings.appTheme)
              || themes[0].items[0]}
            onSelected={(item) => {
              this.props.actions.updateAppTheme(item.item.value);
            }}
          />
        </div>
      </ContentWrapper>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    settings: state.settings
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(settingsActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);
