export const UPDATE_APP_SETTINGS = 'updateAppSettings';
export const UPDATE_AUTORESIZE = 'autoresize';
export const UPDATE_WIDTHINPX = 'widthinpx';
export const UPDATE_ISFULLPAGE = 'isFullPage';
export const UPDATE_RESIZE = 'resize';
export const UPDATE_SIZETOPARENT = 'sizeToParent';
export const UPDATE_MARGIN = 'margin';
export const UPDATE_BASE = 'base';
export const UPDATE_ATLASKIT = 'atlaskit';
export const UPDATE_TIMEOUT_APP_IFRAMES = 'timeoutAppIframes';
export const UPDATE_EXPIRED_JWT = 'expiredJWT';
export const UPDATE_PRODUCT_THEME = 'productTheme';
export const UPDATE_APP_THEME = 'appTheme';

export const updateAppSettings = (event) => {
  return {
    type: UPDATE_APP_SETTINGS,
    event
  };
};

export const updateAutoResize = (event) => {
  return {
    type: UPDATE_AUTORESIZE,
    event
  };
};

export const updateWidthInPx = (event) => {
  return {
    type: UPDATE_WIDTHINPX,
    event
  };
};

export const updateIsFullPage = (event) => {
  return {
    type: UPDATE_ISFULLPAGE,
    event
  };
};

export const updateResize = (event) => {
  return {
    type: UPDATE_RESIZE,
    event
  };
};

export const updateSizeToParent = (event) => {
  return {
    type: UPDATE_SIZETOPARENT,
    event
  };
};

export const updateMargin = (event) => {
  return {
    type: UPDATE_MARGIN,
    event
  };
};

export const updateBase = (event) => {
  return {
    type: UPDATE_BASE,
    event
  };
};

export const updateAtlaskit = (event) => {
  return {
    type: UPDATE_ATLASKIT,
    event
  };
};

export const updateTimeoutAppIframes = (event) => {
  return {
    type: UPDATE_TIMEOUT_APP_IFRAMES,
    event
  };
};

export const updateExpiredJWT = (event) => {
  return {
    type: UPDATE_EXPIRED_JWT,
    event
  };
};

export const updateProductTheme = (event) => {
  return {
    type: UPDATE_PRODUCT_THEME,
    event
  };
};

export const updateAppTheme = (event) => {
  return {
    type: UPDATE_APP_THEME,
    event
  };
};
