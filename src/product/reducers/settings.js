import * as types from '../actions/settings';

export default (state = {}, payload) => {
  switch (payload.type) {
    case types.UPDATE_APP_SETTINGS:
    case types.UPDATE_AUTORESIZE:
    case types.UPDATE_WIDTHINPX:
    case types.UPDATE_ISFULLPAGE:
    case types.UPDATE_RESIZE:
    case types.UPDATE_SIZETOPARENT:
    case types.UPDATE_MARGIN:
    case types.UPDATE_BASE:
    case types.UPDATE_TIMEOUT_APP_IFRAMES:
    case types.UPDATE_EXPIRED_JWT:
      return Object.assign({}, state, {[payload.type]: payload.event.target.checked});
    case types.UPDATE_APP_THEME:
    case types.UPDATE_PRODUCT_THEME:
      return Object.assign({}, state, {[payload.type]: payload.event});
    case types.UPDATE_ATLASKIT:
      return Object.assign({}, state, {[payload.type]: payload.event.items});
    default:
      return state;
  }
};
