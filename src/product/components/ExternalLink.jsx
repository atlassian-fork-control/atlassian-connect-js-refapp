import React, {PureComponent} from "react";
import ShortcutIcon from '@atlaskit/icon/glyph/shortcut';

export default class ExternalLink extends PureComponent {

  render() {
    return (
      <span>
        <a
          href={this.props.href}
          target="_blank"
          rel="noopener noreferrer"
        >
          {this.props.children ? this.props.children : this.props.path}
          &nbsp;<ShortcutIcon size="small" label={this.props.label ? this.props.label : ''} />
        </a>
      </span>
    );
  }

}
