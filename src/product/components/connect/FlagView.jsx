import React, { PureComponent } from 'react';
import Flag, { AutoDismissFlag, FlagGroup } from '@atlaskit/flag';
import '@atlaskit/css-reset';
import GreenSuccessIcon from '../acjs/flagIcons/GreenSuccessIcon';
import PurpleInfoIcon from '../acjs/flagIcons/PurpleInfoIcon';
import YellowWarningIcon from '../acjs/flagIcons/YellowWarningIcon';
import RedErrorIcon from '../acjs/flagIcons/RedErrorIcon';

export class FlagWrapper extends PureComponent {
  render() {
    const flagCreationOptions = this.props.flagCreationOptions;
    const appearance = 'normal';
    const FlagType = flagCreationOptions.close === 'auto' ? AutoDismissFlag : Flag;
    const icon =
      flagCreationOptions.type === 'success' ? <GreenSuccessIcon/> :
        flagCreationOptions.type === 'warning' ? <YellowWarningIcon/> :
          flagCreationOptions.type === 'error' ? <RedErrorIcon/> : <PurpleInfoIcon/>;
    const isDismissAllowed = flagCreationOptions.close === 'never' ? false : this.props.isDismissAllowed;
    return (
      <FlagType
        id={flagCreationOptions.id}
        key={flagCreationOptions.id}
        title={flagCreationOptions.title}
        description={flagCreationOptions.description}
        actions={flagCreationOptions.actions}
        appearance={appearance}
        icon={icon}
        isDismissAllowed={isDismissAllowed}
        onDismissed={this.props.onDismissed}
      />
    );
  }
}

export default class FlagView extends PureComponent {
  render() {
    return (
      <FlagGroup onDismissed={this.props.onDismissed}>
        {
          this.props.allFlagCreationOptions.map(flagCreationOptions => (
            <FlagWrapper
              key={flagCreationOptions.id}
              flagCreationOptions={flagCreationOptions}
            />
          ))
        }
      </FlagGroup>
    );
  }
}
