import React, { PureComponent } from 'react';

import { colors } from '@atlaskit/theme';
import Button, { ButtonGroup } from '@atlaskit/button';
import { ConnectIframe } from '@atlassian/connect-module-core';
import ModalDialog, { ModalFooter } from '@atlaskit/modal-dialog';

import styled from 'styled-components';
import IframeFailedIndicator from '../../components/IframeFailedIndicator';
import IframeLoadingIndicator from '../../components/IframeLoadingIndicator';
import IframeTimeoutIndicator from '../../components/IframeTimeoutIndicator';

export class DialogWrapper extends PureComponent {

  getButtonAppearance = key => {
    if (key === 'submit') {
      return 'primary';
    } else if (key === 'cancel') {
      return 'link';
    } else {
      return 'subtle-link';
    }
  };

  render() {
    const header = (this.props.options.chrome && this.props.options.header) ?
      this.props.options.header : null;

    const Hint = styled.span`
      color: ${colors.subtleText};
    `;

    const footer = () => (this.props.options.chrome && this.props.options.actions) ?
      <ModalFooter>
        <Hint>{this.props.options.hint}</Hint>
        <ButtonGroup>{this.props.options.actions.map(action => (action.hidden ? null :
        <Button
          id={action.identifier}
          key={action.key}
          appearance={this.getButtonAppearance(action.key)}
          onClick={action.onClick}
          isDisabled={action.disabled}
        >
          {action.text}
        </Button>
        ))}</ButtonGroup>
      </ModalFooter> :
      null;

    const onClose = this.props.onDialogDismissed ?
      this.props.onDialogDismissed : () => {};

    //This is now permanent unless a HEIGHT enum becomes available in AtlasKit in the future
    //ie. Products now decide what small/medium/large means for a dialog height
    //ref: AK-1723
    const HEIGHT_ENUM = {
      values: ['small', 'medium', 'large', 'xlarge', 'x-large'],
      heights: {
        small: '400px',
        medium: '600px',
        large: '800px',
        xlarge: '968px',
        'x-large': '968px'
      }
    };

    const height = HEIGHT_ENUM.values.indexOf(this.props.options.height) === -1 ?
      this.props.options.height : HEIGHT_ENUM.heights[this.props.options.height];

    return (
      <ModalDialog
        isOpen={true}
        heading={header}
        footer={footer}
        isChromeless={!this.props.options.chrome}
        width={this.props.options.width}
        onClose={onClose}
        children={<ConnectIframe
          connectHost={this.props.connectHost}
          height={height}
          width={'100%'}
          appKey={this.props.extension.addon_key}
          moduleKey={this.props.extension.key}
          failedToLoadIndicator={IframeFailedIndicator}
          loadingIndicator={IframeLoadingIndicator}
          timeoutIndicator={IframeTimeoutIndicator}
          url={this.props.extension.url}
          options={Object.assign({
            autoresize: this.props.settings.autoresize,
            widthinpx: this.props.settings.widthinpx,
            isDialog: true,
            dialogId: this.props.options.id
          }, this.props.extension.options)}
          connectIframeProvider={this.props.connectIframeProvider}
        />}
      />
    );
  }
}

export default class DialogView extends PureComponent {

  render() {
    return(
      <div>
        {this.props.dialogs.map(dialog => (
          <DialogWrapper
            key={dialog.options.id}
            options={dialog.options}
            extension={dialog.extension}
            settings={this.props.settings}
            connectHost={this.props.connectHost}
            onDialogDismissed={this.props.onDialogDismissed}
            connectIframeProvider={this.props.connectIframeProvider}
          />
        ))}
      </div>
    );
  }
}
