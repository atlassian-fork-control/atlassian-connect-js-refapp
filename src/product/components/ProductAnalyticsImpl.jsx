import {AnalyticsAPI} from '@atlassian/connect-module-core';

class ProductAnalyticsImpl implements AnalyticsAPI {

  trigger(category, action, payload): void {
    console.log(`ACJS Refapp Analytics: [${category}, ${action}, ${payload}]`);
  }

  markAsSafe = (...allowedStrings:string[]) => {
    return (value:string) => {
      return allowedStrings.includes(value) ? value : 'redacted';
    }
  };

  dangerouslyCreateSafeString = (value:string) => value;

}

export default ProductAnalyticsImpl;
