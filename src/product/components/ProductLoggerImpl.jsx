import {LoggerAPI} from '@atlassian/connect-module-core';

export class ProductLoggerImpl implements LoggerAPI {

  debug(message, ...optionalParams) {
    console.debug(`ACJS ${document.title}: Debug: ${message}`, ...optionalParams);
  }

  info(message?: string, ...optionalParams: any[]): void {
    console.info(`ACJS ${document.title}: Info: ${message}`, ...optionalParams);
  }

  warn(message?: string, ...optionalParams: any[]): void {
    console.warn(`ACJS ${document.title}: Warn: ${message}`, ...optionalParams);
  }

  error(message?: string, ...optionalParams: any[]): void {
    console.error(`ACJS ${document.title}: Error: ${message}`, ...optionalParams);
  }

}

export default ProductLoggerImpl;
