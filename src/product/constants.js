import { akGridSize } from '@atlaskit/util-shared-styles';
const gridSizeInt = parseInt(akGridSize, 10);
const settings = {
  autoresize: false,
  widthinpx: true,
  isFullPage: false,
  resize: false,
  sizeToParent: false,
  margin: false,
  base: false,
  atlaskit: [
    { content: 'flag', value: 'flag'},
    { content: 'dialog', value: 'dialog'},
    { content: 'dropdown', value: 'dropdown'},
    { content: 'inlineDialog', value: 'inlineDialog'},
    { content: 'messages', value: 'messages'},
  ],
  productTheme: 'light',
  appTheme: 'light'
};
const appOptionsURL = 'https://developer.atlassian.com/cloud/confluence/about-the-javascript-api/#options';
const iframeMaxWidth = '900px';
const iframeMaxHeight = '900px';
const expiredJWT = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE1MTkyOTc5MDMsImV4cCI6MTUxOTI5NzkwNSwiYXVkIjoiIiwic3ViIjoiIn0.JJc3kOoDmIuY9X864gGfDpVbcItKNsTdCyQZUQ_H6uQ';
const unexpiredJWT = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE1MTkyOTc5MDMsImV4cCI6NDA3NTQ0MTkwNSwiYXVkIjoiIiwic3ViIjoiIn0.T_KWDDZ1Ro62Rmj4946Pdk5SNKg2o-g4e_CMbETLPyk';
export { gridSizeInt, settings, appOptionsURL, iframeMaxWidth, iframeMaxHeight, expiredJWT, unexpiredJWT };