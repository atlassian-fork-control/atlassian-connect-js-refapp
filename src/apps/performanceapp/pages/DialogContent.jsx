import React, { PureComponent } from "react";
import Page from '@atlaskit/page';

/* global AP */

export default class DialogContent extends PureComponent {

  componentWillMount() {
    AP.events.emit('performance.dialog.loaded', {moduleKey: this.props.moduleKey});
  }

  render() {
    return (
      <Page>
       <p>Dialog Loaded</p>
      </Page>
    );
  }
}