import Button from '@atlaskit/button';
import React, { Component } from 'react';
import Page from '@atlaskit/page';

import PageTitle from '../components/PageTitle';
import PerformanceAppUtil from '../../../common/PerformanceappUtil';
import ContentWrapper from '../components/ContentWrapper';

/* global AP */

export default class DialogPage extends Component {
  static performance = {};
  state = {
    performanceTimings: {}
  };

  createDialog = function(moduleKey) {
    DialogPage.performance[moduleKey] = PerformanceAppUtil.currentTime();
    AP.dialog.create({
      key: moduleKey,
      chrome: true
    });
  }

  static dialogLoaded = function(params) {
    const loadingTime = PerformanceAppUtil.currentTime() - DialogPage.performance[params.moduleKey];
    const {performanceTimings} = this.state;
    performanceTimings[params.moduleKey] = loadingTime;
    this.setState({performanceTimings});
  }

  componentWillMount() {
    AP.events.on('performance.dialog.loaded', DialogPage.dialogLoaded.bind(this));
  }
  componentWillUnmount() {
    AP.events.off('performance.dialog.loaded', DialogPage.dialogLoaded.bind(this));
  }

  render() {

    return (
      <ContentWrapper>
        <PageTitle>Cacheable Dialog</PageTitle>
        <Page>
          <Button onClick={() => this.createDialog('perf-dialog')}>Normal</Button>
          <Button onClick={() => this.createDialog('perf-dialog-cacheable')}>Cacheable</Button>
          <h2>Results</h2>
          <p>Original Dialog (in ms): <span>{this.state.performanceTimings['perf-dialog']}</span></p>
          <p>Cacheable Dialog (in ms): <span>{this.state.performanceTimings['perf-dialog-cacheable']}</span></p>
        </Page>

      </ContentWrapper>
    );
  }
}
