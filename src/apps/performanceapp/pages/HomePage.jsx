import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {acjsRefappName} from '../../../common/CommonConstants';
import InstallAppPanel from '../../shared/InstallAppPanel';
import SharedAppUtil from '../../shared/SharedAppUtil';
import ContentWrapper from '../components/ContentWrapper';
import PageTitle from '../components/PageTitle';

export default class HomePage extends PureComponent {

  static contextTypes = {
    showModal: PropTypes.func,
    addFlag: PropTypes.func,
  };

  state = {
    hostProductName: acjsRefappName
  };

  componentWillMount() {
    SharedAppUtil.iterateAppOptions((key, value) => {
      if (key === 'host-product-name') {
        this.setState({hostProductName: value});
      }
    });
  }

  renderInstallAppPanel = () => {
    const jiraDescriptorUrl = SharedAppUtil.buildAppDescriptorUrl('performance-jira-app-connect-descriptor.json');
    const confluenceDescriptorUrl = SharedAppUtil.buildAppDescriptorUrl('performance-confluence-app-connect-descriptor.json');
    return (
      <InstallAppPanel
        appName="Performance App"
        appDescriptorUrls={[jiraDescriptorUrl, confluenceDescriptorUrl]}
      />
    );
  };

  render() {
    return (
      <ContentWrapper>
        <PageTitle>Performance App Home</PageTitle>

        <p>This is a reference app to show various performance timings</p>
        <h3>Installing this App</h3>
        <p>
          {this.renderInstallAppPanel()}
        </p>

      </ContentWrapper>
    );
  }
}
