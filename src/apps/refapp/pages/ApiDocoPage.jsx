import React, { Component } from "react";
import ReactDOM from "react-dom";
import MonacoEditor from 'react-monaco-editor';
import Button from '@atlaskit/button';
import PlayIcon from '@atlaskit/icon/glyph/vid-play';
import SingleSelect from '@atlaskit/single-select';
import AkSpinner from '@atlaskit/spinner';

const documentation = [
  {
    "title": "AP",
    "url": "https://developer.atlassian.com/cloud/confluence/jsapi/ap/"
  },
  {
    "title": "Cookie",
    "url": "https://developer.atlassian.com/cloud/confluence/jsapi/cookie/"
  },
  {
    "title": "Dialog",
    "url": "https://developer.atlassian.com/cloud/confluence/jsapi/dialog/"
  },
  {
    "title": "Events",
    "url": "https://developer.atlassian.com/cloud/confluence/jsapi/events/"
  },
  {
    "title": "Flag",
    "url": "https://developer.atlassian.com/cloud/confluence/jsapi/flag/"
  },
  {
    "title": "Inline dialog",
    "url": "https://developer.atlassian.com/cloud/confluence/jsapi/inline-dialog/"
  },
  {
    "title": "Messages",
    "url": "https://developer.atlassian.com/cloud/confluence/jsapi/messages/"
  },
  {
    "title": "Request",
    "url": "https://developer.atlassian.com/cloud/confluence/jsapi/request/"
  },
  {
    "title": "User",
    "url": "https://developer.atlassian.com/cloud/confluence/jsapi/user/"
  }
];
const querySelectorAll = '#docs';
const querySelectorProxy = 'https://afternoon-gorge-69359.herokuapp.com/cqp';
const editorOptions = {
  selectOnLineNumbers: true,
  minimap: {
    enabled: false
  }
};

export default class ApiDocoPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      module: {}
    }
  }
  loadDoc = ({ item: { content: title, value: url } }) => {
    const module = { title, url };
    fetch(`${querySelectorProxy}?url=${
        encodeURIComponent(url)
      }&querySelectorAll=${
        encodeURIComponent(querySelectorAll)
      }`).then(response => {
        response.text().then(doco => {
          this.setState({ doco, loading: false });
        });
      })
    this.setState({ module, doco: '', loading: true });
  }
  componentDidUpdate() {
    [].forEach.call(
      document.querySelectorAll('.code-example-container'),
      example => {
        const code = example.querySelector('pre').innerText;
        const numLines = code.match(/\n|$/g).length
        let editor;
        ReactDOM.render(
          <div>
            <div
              style={{
                position: 'absolute',
                right: 0,
                zIndex: 1
              }}
            >
              <Button
                appearance="primary"
                iconBefore={<PlayIcon label="Run" />}
                // eslint-disable-next-line
                onClick={() => editor && eval(editor.getValue())}
              >Run</Button>
            </div>
            <MonacoEditor
              height={numLines * 18}
              options={editorOptions}
              language="javascript"
              theme="vs-dark"
              value={code}
              editorDidMount={monaco => editor = monaco}
            />
          </div>
          , example);
      });
  }
  render() {
    const selectItems = [{
      items: documentation.map(module => (
        {
          content: module.title,
          value: module.url
        }
      ))
    }];
    return (
      <div>
        <style dangerouslySetInnerHTML={{
          __html: `
          @font-face {
            font-family: CircularBold;
            src: url("/lineto-circular-bold-c.eot");
            src: url("/lineto-circular-bold-c.eot?#iefix") format("embedded-opentype"),
            url("/lineto-circular-bold-c.woff") format("woff"),
            url("/lineto-circular-bold-c.ttf") format("truetype"),
            url("/lineto-circular-bold-c.svg#svgFontName") format("svg")
          }
          @font-face {
            font - family: CircularBook;
            src: url("/lineto-circular-book-c.eot");
            src: url("/lineto-circular-book-c.eot?#iefix") format("embedded-opentype"),
            url("/lineto-circular-book-c.woff") format("woff"),
            url("/lineto-circular-book-c.ttf") format("truetype"),
            url("/lineto-circular-book-c.svg#svgFontName") format("svg")
          }
        `}} />
        <div style={{
          width: '12em'
        }}>
          <SingleSelect
            placeholder="Select a module"
            items={selectItems}
            onSelected={this.loadDoc}
            shouldFitContainer={true}
            noMatchesFound=""
          />
        </div>
        <div
          style={{
            width: '46em',
            margin: 'auto'
          }}
        >
          <h1
            style={{
              lineHeight: '1.4',
              color: 'rgb(43, 61, 92)',
              fontFamily: 'CircularBold',
              margin: '0px 0px 12px'
            }}
          >{this.state.module.title}</h1>
          {this.state.loading && <AkSpinner size='large' />}
          <div dangerouslySetInnerHTML={{ __html: this.state.doco }} />
        </div>
      </div>
    );
  }
}
