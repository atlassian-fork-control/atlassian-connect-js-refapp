import React, { PureComponent } from "react";
import Button from "@atlaskit/button";
import ContentWrapper from "../components/ContentWrapper";
import PageTitle from "../components/PageTitle";

/* global AP */

class DropdownControllerUi extends PureComponent {

  state = {
    anchorWidth: 400
  };

  createMyDropdown = () => {
    AP.dropdown.create({
      dropdownId: this.props.dropdownId,
      list: this.props.list
    });
  };

  showMyDropdown = () => {
    const selector = '.' + this.props.dropdownAnchorClassName;
    const rect = document.querySelector(selector).getBoundingClientRect();
    AP.dropdown.showAt(this.props.dropdownId, rect.left, rect.top, rect.width);
  };

  hideMyDropdown = () => {
    AP.dropdown.hide(this.props.dropdownId);
  }

  toggleEnableItems = (enabled) => {
    // recursive function to search nested lists and enable all istems
    let counter = 0;
    let method = AP.dropdown['item' + (enabled ? 'Enable' : 'Disable')];
    function setAllInList(list, dropdownId){
      list.forEach((listItem) => {
        if(listItem.list) {
          return setAllInList(listItem.list, dropdownId, enabled);
        } else if(listItem.itemId) {
          method(dropdownId, listItem.itemId);
        } else {
          method(dropdownId, counter);
        }
        counter++;
      }, this);
    }
    setAllInList(this.props.list, this.props.dropdownId, enabled);
  }

  enableDropdownItems = () => {
    this.toggleEnableItems(true);
  }

  disableDropdownItems = () => {
    this.toggleEnableItems(false);
  }

  increaseAnchorWidth = () => {
    this.setState({
      anchorWidth: this.state.anchorWidth + 50
    });
  }

  decreaseAnchorWidth = () => {
    this.setState({
      anchorWidth: Math.max(this.state.anchorWidth - 50, 0)
    });
  }

  render() {
    return (
      <div>
        <h5>{this.props.title}</h5>

        <h6>Controls</h6>
        <Button
          appearance="primary"
          onClick={this.createMyDropdown}
        >
          Create
        </Button>
        &nbsp;
        <Button
          appearance="primary"
          onClick={this.showMyDropdown}
        >
          Show
        </Button>
        &nbsp;
        <Button
          appearance="primary"
          onClick={this.hideMyDropdown}
        >Hide</Button>
        &nbsp;
        <Button
          appearance="primary"
          onClick={this.enableDropdownItems}
        >Enable disabled items</Button>
        &nbsp;
        <Button
          appearance="primary"
          onClick={this.disableDropdownItems}
        >Disable dropdown items</Button>

        <h6>Anchor</h6>
        <Button
          appearance="default"
          onClick={this.increaseAnchorWidth}
        >
          Increase width
        </Button>
        &nbsp;
        <Button
          appearance="default"
          onClick={this.decreaseAnchorWidth}
        >
          Decrease width
        </Button>
        <div
          className={this.props.dropdownAnchorClassName}
          style={{
            height: '40px',
            width: this.state.anchorWidth + 'px',
            border: '1px solid #ccc',
            borderRadius: '3px',
            marginTop: '10px'
          }}
        >
        </div>
      </div>
    );
  }
}

export default class DropdownPage extends PureComponent {

  constructor(props) {
    super(props);

    AP.events.on('dropdown-shown', (data) =>{
      this.logEvent(`dropdown with id ${data.dropdownId} shown`);
    });

    AP.events.on('dropdown-hide', (data) =>{
      this.logEvent(`dropdown with id ${data.dropdownId} hidden`);
    });

    AP.events.on('dropdown-item-selected', (data) =>{
      this.logEvent(`dropdown with id ${data.dropdownId} selected the item ${data.item.id}`);
    });

  }

  logEvent = (message) => {
    AP.flag.create({
      title: 'Dropdown event',
      body: message,
      type: 'success',
      close: 'auto'
    });
  }

  render() {
    return (
      <ContentWrapper>
        <PageTitle>Dropdown</PageTitle>

        <DropdownControllerUi
          title="Simple"
          dropdownId="simple-dropdown"
          dropdownAnchorClassName="simple-dropdown"
          list={[
            {
              list: [
                'simple one',
                'simple two',
                'simple three'
              ]
            }
          ]}
          logEvent={this.logEvent}
        />

        <hr/>

        <DropdownControllerUi
        title="Complex"
        dropdownId="complex-dropdown"
        dropdownAnchorClassName="complex-dropdown"
        list={[
          {list: ['item 1']},
          {
            heading: 'section heading',
            list: [
              'item 1.1',
              'item 1.2',
              {
                itemId: 'dropdownid-one',
                text: 'item 1.2.3 (initially disabled)',
                icon: 'view',
                disabled: true
              }
            ]
          },
          {list: ['item 2']},
        ]}
        logEvent={this.logEvent}
      />

      </ContentWrapper>
    );
  }
}
