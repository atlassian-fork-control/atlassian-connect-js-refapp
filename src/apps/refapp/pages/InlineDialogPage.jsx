import React, { PureComponent } from 'react';
import Button from '@atlaskit/button';
import PageTitle from '../components/PageTitle';
import ContentWrapper from '../components/ContentWrapper';

/* global AP */

export default class InlineDialogPage extends PureComponent {
  render() {
    return (
      <ContentWrapper>
        <PageTitle>Inline Dialog</PageTitle>
        <br />
        <Button onClick={() => AP.inlineDialog.hide()}>AP.inlineDialog.hide()</Button>
        <p>
          Note: clicking this won't have any effect <br /> if the current add-on is not within an inline dialog.
        </p>
      </ContentWrapper>
    );
  }
}
