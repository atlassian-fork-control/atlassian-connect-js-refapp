import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@atlaskit/button';
import FieldText from '@atlaskit/field-text';
import PageTitle from '../components/PageTitle';
import ContentWrapper from '../components/ContentWrapper';

/* global AP */

export default class SizingPage extends Component {

  static contextTypes = {
    width: PropTypes.string,
    height: PropTypes.string,
    minWidth: PropTypes.string,
    minHeight: PropTypes.string,
    onWidthChange: PropTypes.func,
    onHeightChange: PropTypes.func,
  };

  render() {
    return (
      <ContentWrapper>
        <PageTitle>Sizing</PageTitle>
        <FieldText
          type='number'
          label='App height (px)'
          value={this.context.height}
          onChange={this.context.onHeightChange}
          isInvalid={this.context.height < this.context.minHeight}
        />
        <FieldText
          type='number'
          label='App width (px)'
          value={this.context.width}
          onChange={this.context.onWidthChange}
          isInvalid={this.context.width < this.context.minWidth}
        />
        <br/>
        <Button onClick={() => AP.sizeToParent()}>AP.sizeToParent()</Button>
        &nbsp;
        <Button onClick={() => AP.resize()}>AP.resize()</Button>
        <br/>
      </ContentWrapper>
    );
  }
}
