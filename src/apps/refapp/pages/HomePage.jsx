import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {acjsRefappName} from '../../../common/CommonConstants';
import InstallAppPanel from '../../shared/InstallAppPanel';
import SharedAppUtil from '../../shared/SharedAppUtil';
import ContentWrapper from '../components/ContentWrapper';
import PageTitle from '../components/PageTitle';

export default class HomePage extends PureComponent {

  static contextTypes = {
    showModal: PropTypes.func,
    addFlag: PropTypes.func,
  };

  state = {
    hostProductName: acjsRefappName
  };

  componentWillMount() {
    SharedAppUtil.iterateAppOptions((key, value) => {
      if (key === 'host-product-name') {
        this.setState({hostProductName: value});
      }
    });
  }

  renderInstallAppPanel = () => {
    const jiraDescriptorUrl = SharedAppUtil.buildAppDescriptorUrl('kitchen-sink-jira-app-connect-descriptor.json');
    const confluenceDescriptorUrl = SharedAppUtil.buildAppDescriptorUrl('kitchen-sink-confluence-app-connect-descriptor.json');
    return (
      <InstallAppPanel
        appName="Kitchen Sink App"
        appDescriptorUrls={[jiraDescriptorUrl, confluenceDescriptorUrl]}
      />
    );
  };

  render() {
    return (
      <ContentWrapper>
        <PageTitle>Kitchen Sink App Home</PageTitle>

        <h3>About the Kitchen Sink App</h3>
        <p>
          This is a reference app to demonstrate a wide variety of functionality available to apps.
        </p>
        <p>
          Detected the host product is <strong>{this.state.hostProductName}</strong>.
        </p>
        <p>
          Select a module menu item at the left.
        </p>

        <h3>Installing this App</h3>
        <p>
          {this.renderInstallAppPanel()}
        </p>

      </ContentWrapper>
    );
  }
}
