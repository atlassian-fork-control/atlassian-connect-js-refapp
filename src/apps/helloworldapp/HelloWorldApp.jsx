import React, { PureComponent } from 'react';

export default class HelloWorldApp extends PureComponent {

  render() {
    return (
      <div>
        Hello World
      </div>
    );
  }
}
