const globals = require('../globals');

class KitchenSinkRefAppPage {
  get dialogMenuItem() {return $('span=Dialog')}
  get flagMenuItem() {return $('span=Flag')}
  get messageMenuItem() {return $('span=Message')}
  get dropdownMenuItem() {return $('span=Dropdown')}
  get nestedAppMenuItem() {return $('span=Nested App')}
  get createDialogButton() {return $('button=Create dialog')}
  get createFlagButton() {return $('button=Create flag')}
  get createMessageButton() {return $('button=Create message')}
  get createDropdownButton() {return $('button=Create')}
  get showDropdownButton() {return $('button=Show')}
  get hideDropdownButton() {return $('button=Hide')}
  get kitchenSinkAppVisible() {return browser.elements(globals.kitchen_sink_refapp_selector).value.length === 1}
}

module.exports = new KitchenSinkRefAppPage();