const assert = require('assert');
const globals = require('../globals');

class ProductPage {
    get kitchenSinkAppMenuItem() {return browser.element('.//span[text()="Kitchen Sink Page"]')}
    get requestAppMenuItem() {return browser.element('.//span[text()="Request Page"]')}
    get inlineDialogMenuItem() {return browser.element('.//span[text()="Full inline dialog"]')}
    get dropdown() {return './/select'}
    get defaultFlag() {return './/span[contains(.,"This is an \'info\' flag.")]'}
    get defaultMessage() {return './/span[contains(.,"Message 1: Test message")]'}
    get numberOfDialogIframes() {return browser.elements(globals.dialog_selector).value.length}
    get numberOfRefAppIframes() {return browser.elements(globals.kitchen_sink_refapp_selector).value.length}
    get numberOfInlineDialogIframes() {return browser.elements(globals.inline_dialog_selector).value.length}

    open() {
      browser.url('/');
    }
}

module.exports = new ProductPage();