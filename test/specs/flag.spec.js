const globals = require('../globals');
const RefAppPage = require('../pageobjects/KitchenSinkRefAppPage');
const ProductPage = require('../pageobjects/ProductPage');

describe('Flag page', () => {

  beforeAll(() => {
    ProductPage.open();
    browser.waitForVisibleAndClick(ProductPage.kitchenSinkAppMenuItem);

    browser.selectFrame(globals.kitchen_sink_refapp_selector);
    browser.waitForVisibleAndClick(RefAppPage.flagMenuItem);
  });

  describe('Create flag button', () => {
    it('should launch a flag', () => {
      browser.waitForVisibleAndClick(RefAppPage.createFlagButton);

      browser.selectTopWindow();
      browser.waitUntil(
        () => browser.isVisible(ProductPage.defaultFlag),
        browser.getConfig().waitForTimeout,
        `Flag was not found on page with selector: ${ProductPage.defaultFlag.selector}`
      );
    });
  });
});