const globals = require('../globals');
const RefAppPage = require('../pageobjects/KitchenSinkRefAppPage');
const ProductPage = require('../pageobjects/ProductPage');

describe('Dropdown page', () => {

  beforeAll(() => {
    ProductPage.open();
    browser.waitForVisibleAndClick(ProductPage.kitchenSinkAppMenuItem);

    browser.selectFrame(globals.kitchen_sink_refapp_selector);
    browser.waitForVisibleAndClick(RefAppPage.dropdownMenuItem);
  });

  describe('Dropdown menu', () => {
    it('should be on the product page', () => {
      browser.selectTopWindow();
      expect(browser.isExisting(ProductPage.dropdown)).toEqual(false);

      browser.selectFrame(globals.kitchen_sink_refapp_selector);
      browser.waitForVisibleAndClick(RefAppPage.createDropdownButton);
      browser.waitForVisibleAndClick(RefAppPage.showDropdownButton);

      browser.selectTopWindow();
      browser.waitUntil(
        () => browser.isExisting(ProductPage.dropdown),
        browser.getConfig().waitForTimeout,
        `Dropdown item was not found on parent page with selector: ${ProductPage.dropdown.selector}`
      );

      browser.selectFrame(globals.kitchen_sink_refapp_selector);
      browser.waitForVisibleAndClick(RefAppPage.hideDropdownButton);
    });
  });
});