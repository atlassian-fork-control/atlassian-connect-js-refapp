const globals = require('../globals');
const RequestAppPage = require('../pageobjects/RequestAppPage');
const ProductPage = require('../pageobjects/ProductPage');

describe('Request page', () => {

  beforeAll(() => {
    ProductPage.open();
    browser.waitForVisibleAndClick(ProductPage.requestAppMenuItem);


    browser.selectFrame(globals.request_refapp_selector);
  });

  describe('Send request', () => {
    it('should send a request', () => {
      browser.waitForVisibleAndClick(RequestAppPage.sendRequestButton);

      browser.selectTopWindow();
      browser.waitUntil(
        () => browser.isVisible(RequestAppPage.requestSentSuccessfullyFlag),
        browser.getConfig().waitForTimeout,
        `Flag was not found indicating the request was sent successfully. Selector used is: ${RequestAppPage.requestSentSuccessfullyFlag.selector}`
      );
    });
  });
});