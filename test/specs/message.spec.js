const globals = require('../globals');
const RefAppPage = require('../pageobjects/KitchenSinkRefAppPage');
const ProductPage = require('../pageobjects/ProductPage');

describe('Message page', () => {

  beforeAll(() => {
    ProductPage.open();
    browser.waitForVisibleAndClick(ProductPage.kitchenSinkAppMenuItem);

    browser.selectFrame(globals.kitchen_sink_refapp_selector);
    browser.waitForVisibleAndClick(RefAppPage.messageMenuItem);
  });

  describe('Create message button', () => {
    it('should launch a message', () => {
      browser.waitForVisibleAndClick(RefAppPage.createMessageButton);

      browser.selectTopWindow();
      browser.waitUntil(
        () => browser.isVisible(ProductPage.defaultMessage),
        browser.getConfig().waitForTimeout,
        `Message was not found on page with selector: ${ProductPage.defaultMessage.selector}`
      );
    });
  });
});