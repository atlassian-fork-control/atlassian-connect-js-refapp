const assert = require('assert');
const globals = require('../globals');
const RefAppPage = require('../pageobjects/KitchenSinkRefAppPage');
const ProductPage = require('../pageobjects/ProductPage');

describe('iframe', () => {

  beforeAll(() => {
    ProductPage.open();
    browser.waitForVisibleAndClick(ProductPage.kitchenSinkAppMenuItem);

    browser.selectFrame(globals.kitchen_sink_refapp_selector);
    browser.waitForVisibleAndClick(RefAppPage.nestedAppMenuItem);
  });

  describe('iframe-in-iframe', () => {
    it('should appear in the DOM', () => {
      browser.waitUntil(
        () => RefAppPage.kitchenSinkAppVisible === true,
        browser.getConfig().waitForTimeout,
        'The top level kitchen sink refapp iframe was never added to the DOM'
      );

      browser.selectFrame(globals.kitchen_sink_refapp_selector);
      browser.waitUntil(
        () => RefAppPage.kitchenSinkAppVisible === true,
        browser.getConfig().waitForTimeout,
        'The nested kitchen sink refapp iframe iframe was never added to the DOM'
      );
    });

    it('should establish xdm bridge', () => {
      browser.selectFrame(globals.kitchen_sink_refapp_selector);
      const result = browser.executeAsync(function(done){AP.env.getLocation(function(loc){done(loc)})});
      assert(
        result.value.search(browser.getConfig().baseUrl) === 0,
        'Could not exercise AP.env.getLocation accross the XDM bridge'
      );
    });
  });
});